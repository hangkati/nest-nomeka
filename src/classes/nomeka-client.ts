import { Logger } from "@nestjs/common";
import { ClientProxy, ReadPacket, WritePacket } from "@nestjs/microservices";
import { RmqUrl } from "@nestjs/microservices/external/rmq-url.interface";
import * as rqmPackage from 'amqp-connection-manager';
import { EventEmitter } from "events";
import { EmptyError, first, fromEvent, lastValueFrom, map, merge, Observable, share, switchMap } from "rxjs";
import { NomekaOptions } from "../interfaces/nomeka-options.interface";
import {
  DISCONNECT_EVENT,
  RQM_DEFAULT_EXCHANGE,
  RQM_DEFAULT_IS_GLOBAL_PREFETCH_COUNT,
  RQM_DEFAULT_NOACK,
  RQM_DEFAULT_PREFETCH_COUNT,
  RQM_DEFAULT_QUEUE,
  RQM_DEFAULT_QUEUE_OPTIONS,
  RQM_DEFAULT_URL
} from '../constants';

const REPLY_QUEUE = ''; //'amq.rabbitmq.reply-to';

export class NomekaClient extends ClientProxy {
  protected readonly logger = new Logger(ClientProxy.name);
  protected connection: Promise<any>;
  protected client: rqmPackage.AmqpConnectionManager = null;
  protected channel: rqmPackage.ChannelWrapper = null;
  protected urls: string[] | RmqUrl[];
  protected exchange: string;
  protected queue: string;
  protected queueOptions: any;
  protected responseEmitter: EventEmitter;
  protected replyQueue: string;
  protected persistent: boolean;

  constructor(protected readonly options: NomekaOptions['options']) {
    super();
    this.urls = this.getOptionsProp(this.options, 'urls') || [RQM_DEFAULT_URL];
    this.exchange = this.getOptionsProp(this.options, 'exchange') || RQM_DEFAULT_EXCHANGE;
    this.queue = this.getOptionsProp(this.options, 'queue') || RQM_DEFAULT_QUEUE;
    this.queueOptions = this.getOptionsProp(this.options, 'queueOptions') || RQM_DEFAULT_QUEUE_OPTIONS;
    this.replyQueue = this.getOptionsProp(this.options, 'replyQueue') || REPLY_QUEUE;
  }
  connect(): Promise<any> {
    if (this.client) {
      return this.connection;
    }
    this.client = this.createClient();
    // this.handleError(this.client);
    // this.handleDisconnectError(this.client);

    const connect$ = this.connect$(this.client);
    this.connection = lastValueFrom(
      this.mergeDisconnectEvent(this.client, connect$).pipe(
        switchMap(() => this.createChannel()),
        share(),
      ),
    ).catch(err => {
      if (err instanceof EmptyError) {
        return;
      }
      throw err;
    });

    return this.connection;
  }
  close() {
    this.channel && this.channel.close();
    this.client && this.client.close();
    this.channel = null;
    this.client = null;
  }
  protected publish(packet: ReadPacket<any>, callback: (packet: WritePacket<any>) => void): () => void {
    const correlationId = (Math.random() * 100000).toFixed(0)
    this.channel.publish(
      this.exchange,
      `${this.queue}.${packet.pattern}`,
      Buffer.from(JSON.stringify({
        "args": packet.data, "kwargs": {}
      })),
      {
        correlationId: correlationId,
        replyTo: this.replyQueue,
        contentEncoding: 'utf-8',
        contentType: 'application/json'
      } as any)
    return () => { }
  }
  protected dispatchEvent<T = any>(packet: ReadPacket<any>): Promise<T> {
    throw new Error("Method not implemented.");
  }

  public createClient<T = any>(): rqmPackage.AmqpConnectionManager {
    const socketOptions = this.getOptionsProp(this.options, 'socketOptions');
    return rqmPackage.connect(this.urls, {
      connectionOptions: socketOptions,
    });
  }

  createChannel(): Promise<void> {
    return new Promise(resolve => {
      this.channel = this.client.createChannel({
        json: false,
        setup: (channel: any) => this.setupChannel(channel, resolve),
      });
    });
  }

  public async setupChannel(channel: any, resolve: Function) {
    const prefetchCount = this.getOptionsProp(this.options, 'prefetchCount') || RQM_DEFAULT_PREFETCH_COUNT;
    const isGlobalPrefetchCount = this.getOptionsProp(this.options, 'isGlobalPrefetchCount') || RQM_DEFAULT_IS_GLOBAL_PREFETCH_COUNT;

    await channel.assertQueue(`rpc-${this.queue}`, this.queueOptions);
    await channel.prefetch(prefetchCount, isGlobalPrefetchCount);

    this.responseEmitter = new EventEmitter();
    this.responseEmitter.setMaxListeners(0);
    await this.consumeChannel(channel);
    resolve();
  }

  public async consumeChannel(channel: any) {
    const noAck = this.getOptionsProp(this.options, 'noAck', RQM_DEFAULT_NOACK);
    await channel.consume(
      this.replyQueue,
      (msg: any) => {
        console.log(msg);
        this.responseEmitter.emit(msg.properties.correlationId, msg)
      },
      {
        noAck,
      },
    );
  }

  public mergeDisconnectEvent<T = any>(
    instance: any,
    source$: Observable<T>,
  ): Observable<T> {
    const close$ = fromEvent(instance, DISCONNECT_EVENT).pipe(
      map((err: any) => {
        throw err;
      }),
    );
    return merge(source$, close$).pipe(first());
  }

}
